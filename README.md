# Blaster Faster [2018]

Created a kind of Space Invader game [Followed Tutorial].

## Playable Example
[Play Blaster Faster](https://majorvitec.itch.io/blaster-faster)

## Game Overview

### Source
[Make a Space Shooting Game in Godot](https://www.youtube.com/watch?v=Z9W6dlP-RB8&list=PLv3l-oZCXaqkUEqrLsKJIAhAxK_Im6Qew)

### Start Screen

![Start Screen](/images/readme/start_screen.png "Start Screen")

### Playing Screen

![Playing Screen](/images/readme/play_screen.png "Playing Screen")

