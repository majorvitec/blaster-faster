extends CanvasLayer

const ADD_SCORE: int = 5

func _ready():
	add_to_group('HUD')

func update_score() -> void:
	var prev_score: int = int($text_score/label.text)
	var new_score: int = prev_score + ADD_SCORE

	$text_score/label.text = str(new_score)

func update_armor(armor: int) -> void:
	$spr_armor.frame = armor
