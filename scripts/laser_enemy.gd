# script: laser_enemy

extends "res://scripts/laser.gd"

const ARMOR_VALUE: int = 1

func _ready() -> void:
	connect("area_entered", self, "_on_area_enter")
	audio_player.play("aud_laser_enemy")
	
func _on_area_enter(other) -> void:
	if other.is_in_group("ship"):
		other.armor -= ARMOR_VALUE
		update_armor(other.armor)
		create_flare()
		utils.remote_call("camera", "shake", 1, 0.13)
		queue_free()

func update_armor(armor: int) -> void:
	get_tree().call_group('HUD', 'update_armor', armor)	
