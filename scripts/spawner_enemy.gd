# script: spawner_enemy

extends Node

#const enemy_kamikaze = preload("res://scenes/enemy_kamikaze.tscn")
const ENEMIES: Array = [
	preload("res://scenes/enemy_kamikaze.tscn"),
	preload("res://scenes/enemy_clever.tscn")
]
const MAX_SCREEN_WIDTH: int = 16

func _ready() -> void:
	yield(utils.create_timer(1.2), "timeout")
	spawn()
	

func spawn() -> void:
	while true:
		randomize()
		
		var enemy: Node = utils.choose(ENEMIES).instance()
		var pos: Vector2 = Vector2()
		var left_screen: int = 0 + MAX_SCREEN_WIDTH
		var right_screen: int = utils.view_size.x - MAX_SCREEN_WIDTH
		var top_enemy_pos: int = 0 - MAX_SCREEN_WIDTH
		
		pos.x = rand_range(left_screen, right_screen)
		pos.y = top_enemy_pos
		enemy.set_position(pos)
		get_node("container").add_child(enemy)
		yield(utils.create_timer(rand_range(0.5, 1.25)), "timeout")
