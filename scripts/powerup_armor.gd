# script: powerup_armor

extends "res://scripts/powerup.gd"

const ARMOR_VALUE: int = 1

func _ready() -> void:
	connect("area_entered", self, "_on_area_entered")

func _on_area_entered(other) -> void:
	if other.is_in_group("ship"):
		other.armor += ARMOR_VALUE
		update_armor(other.armor)
		audio_player.play("aud_powerup")
		queue_free()

func update_armor(armor: int):
	get_tree().call_group('HUD', 'update_armor', armor)	