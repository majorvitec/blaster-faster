# script: laser_ship

extends "res://scripts/laser.gd"

const ARMOR_VALUE: int = 1

func _ready() -> void:
	connect("area_entered", self, "_on_area_enter")
	audio_player.play("aud_laser_ship")
	
func _on_area_enter(other) -> void:
	if other.is_in_group("enemy"):
		other.armor -= ARMOR_VALUE
		create_flare()
		utils.remote_call("camera", "shake", 3, 0.13)
		queue_free()
