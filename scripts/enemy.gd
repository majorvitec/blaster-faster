# script: enemy

extends Area2D

export var armor: int = 2 setget set_armor
export var velocity: Vector2 = Vector2()

const SCN_EXPLOSION: Resource = preload("res://scenes/explosion.tscn")
const MAX_SCREEN_HEIGHT: int = 16
const ARMOR_VALUE: int = 1

func _ready() -> void:
	set_process(true)
	add_to_group("enemy")
	connect("area_entered", self, "_on_area_enter")

func _process(delta) -> void:
	translate(velocity * delta)

	var offset: float = position.y - MAX_SCREEN_HEIGHT
	if offset >= get_viewport_rect().size.y:
		queue_free()

func set_armor(new_value) -> void:
	if is_queued_for_deletion():
		return

	if new_value < armor:
		audio_player.play("aud_hit_enemy")

	armor = new_value
	if armor <= 0:
		get_tree().call_group('HUD', 'update_score')
		create_explosion()
		queue_free()


func _on_area_enter(other) -> void:
	if other.is_in_group("ship"):
		other.armor -= ARMOR_VALUE
		update_armor(other.armor)
		create_explosion()
		queue_free()

func create_explosion() -> void:
	var explosion: Node = SCN_EXPLOSION.instance()
	explosion.set_position(position)
	utils.main_node.add_child(explosion)

func update_armor(armor_number: int) -> void:
	get_tree().call_group('HUD', 'update_armor', armor_number)
