# script: spawner_powerup

extends Node

#const enemy_kamikaze = preload("res://scenes/enemy_kamikaze.tscn")
const POWERUPS: Array = [
	preload("res://scenes/powerup_armor.tscn"),
	preload("res://scenes/powerup_laser.tscn")
]

const MAX_SCREEN_WIDTH: int = 7

func _ready() -> void:
	yield(utils.create_timer(rand_range(10, 15)), "timeout")
	spawn()
	
func spawn() -> void:
	while true:
		randomize()
		
		var powerup: Node = utils.choose(POWERUPS).instance()
		var pos: Vector2 = Vector2()
		var left_screen: int = 0 + MAX_SCREEN_WIDTH
		var right_screen: int = utils.view_size.x - MAX_SCREEN_WIDTH
		var top_powerup_pos: int = 0 - MAX_SCREEN_WIDTH
		
		pos.x = rand_range(left_screen, right_screen)
		pos.y = top_powerup_pos
		powerup.set_position(pos)
		get_node("container").add_child(powerup)
		yield(utils.create_timer(rand_range(10, 15)), "timeout")
